from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegisterForm, UserUpdateForm, ProfileUpdateForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator

# needed for email auth
from django.http import HttpResponseBadRequest
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.auth.models import User
from django.urls import reverse

from django.core.mail import EmailMessage
from django.template.loader import render_to_string


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
    
            uidb64 = urlsafe_base64_encode(str(user.pk).encode())
            token = default_token_generator.make_token(user)
            confirmation_link = reverse('confirm_email', args=[uidb64, token])

            send_confirmation_email(user, confirmation_link)

            messages.success(request, f'An email has been sent for account verification.')
            
            return redirect('login')
    else:
        form = UserRegisterForm()

    return render(request, 'users/register.html', {'form': form})


def send_confirmation_email(user, confirmation_link):
    subject = 'Confirm Your Email'
    message = render_to_string('registration_confirmation_email.html', {
        'user': user,
        'confirmation_link': confirmation_link,
    })
    email = EmailMessage(subject, message, to=[user.email])
    email.content_subtype = 'html'  # Set the content type to HTML
    email.send()


def confirm_email(request, uidb64, token):

    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    
    if user and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        messages.success(request, 'Your email has been confirmed! You can now log in.')
        return redirect('login')
    else:
        return HttpResponseBadRequest('Invalid confirmation link')

@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your account has been updated!')
            return redirect('profile') # causes the browser to send a get request
    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }

    return render(request, 'users/profile.html', context)